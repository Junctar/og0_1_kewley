package addon;

public class Addon {
	private int addonNummer;
	private String addonName;
	private double preis;
	private String art;
	private int anz;
	private int maxanz;
	
	public Addon() {

	}

	public Addon(int addonNummer, String addonName, double preis, String art, int anz, int maxanz) {
		addonNummer = this.addonNummer;
		addonName = this.addonName;
		preis = this.preis;
		art = this.art;
		anz = this.anz;
		maxanz = this.maxanz;

	}

	public void aendereBestand(int addonName) {
		anz = addonName;
	}

	public double gesamtwertAddons() {
		return anz * preis;
	}
	 
	public void setAddonNummer(int addonNummer) {
		this.addonNummer = addonNummer;
	}
	
	public int getAddonNummer() {
		return addonNummer;
	}
	
	public void setAddonName(String AddonName) {
		this.addonName = AddonName;
	}
	
	public String getAddonName() {
		return addonName;
	}
	
	public void setPreis(double preis) {
		this.preis = preis;
	}
	
	public double getPreis() {
		return preis;
	}
	
	public void setArt (String art) {
		this.art = art;
	}
	
	public String getArt() {
		return art;
	}
	
	public void setAnz(int anz) {
		this.anz = anz;
	}
	
	public int getAnz() {
		return anz;
	}
	
	public void setMaxanz(int maxanz) {
		this.maxanz = maxanz;
	}
	 public int getMaxanz() {
		 return maxanz;
	 }
	
	
}