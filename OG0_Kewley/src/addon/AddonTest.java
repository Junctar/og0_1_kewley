package addon;
import java.util.Scanner;

public class AddonTest {
	private static int addonNummer;
	private static String addonName;
	private static double preis;
	private static String art;
	private static int anz;
	private static int maxanz;
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geben sie eine Addon Nummer ein: ");
		addonNummer = scanner.nextInt();
		System.out.println("Geben sie einen Addon Namen ein: ");
		addonName = scanner.next();
		System.out.println("Wie teuer ist das Addon ?: ");
		preis = scanner.nextDouble();
		System.out.println("Geben sie die Addonart ein: ");
		art = scanner.next();
		System.out.println("Wie viele Addons m�chtest du kaufen ?: ");
		anz = scanner.nextInt();
		System.out.println("Geben sie eine Maximale Anzahl von dem Addon ein: ");
		maxanz = scanner.nextInt();
		
		
		
		Addon addon1 = new Addon(addonNummer, addonName, preis, art, anz, maxanz);
		System.out.println("Die Addon Nummer lautet: " + addon1.getAddonNummer() + ".");
		System.out.println("Der Name lautet: " + addon1.getAddonName() + ".");
		System.out.println("Das Addon kostet: " + addon1.getPreis() + ".");
		System.out.println("Die Addonart ist: " + addon1.getArt() + ".");
		System.out.println("Sie keufen " + addon1.getAnz() + " Addons.");
		System.out.println("Sie k�nnen maximal " + addon1.getMaxanz() + " Addons besitzen.");
		
		
		
		
		Addon addon2 = new Addon(97483,"Goldenes Tierfutter",0.99,"Futter",1,50);
		System.out.println("Die Addon Nummer lautet: " + addon2.getAddonNummer() + ".");
		System.out.println("Der Name lautet: " + addon2.getAddonName() + ".");
		System.out.println("Das Addon kostet: " + addon2.getPreis() + ".");
		System.out.println("Die Addonart ist: " + addon2.getArt() + ".");
		System.out.println("Sie keufen " + addon2.getAnz() + " Addons.");
		System.out.println("Sie k�nnen maximal " + addon2.getMaxanz() + " Addons besitzen.");
		
		scanner.close();
		
	}

}